import axios from 'axios';
import {AsyncStorage,Alert} from 'react-native';
import GLOBALES from './app.json';
/*const promesa= async (type,data) => {
    let  token=await AsyncStorage.getItem('tokenUsuario');
    const http=axios.create({
        baseURL: `http://192.168.0.103:3000/`,
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      return http;
}*/
const valor=async (type,url,data) => {
    let  token=await AsyncStorage.getItem('tokenUsuario');
    const response=await axios({
        url: GLOBALES.hostApi+url,
        method: type,
        headers: {
            'content-type': 'application/json' ,
            'Authorization': `Bearer ${token}`
        },
        data
    });
    let dtos=response.data;
   // let tokenU=await AsyncStorage.getItem('tokenUsuario');
  
    if(dtos.refrescar==true){
    
      await AsyncStorage.setItem('tokenUsuario',dtos.refrestoken);
    }
    if(dtos.codeError=='EXPIRADO'){
      await AsyncStorage.removeItem('tokenUsuario');    
    }
      return response;
}
//export const HTTP=await promesa();
export const HTTP=valor;

/*
export const HTTP = axios.create({
    baseURL: `http://192.168.0.103:3000/`,
    headers: {
      Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NzgxOTg5NjUsImRhdGEiOiJmMjM1ZWZiOS0xMTU2LTQ2MzMtYmQ2Mi00ZTQwNjZiOTEzMjYiLCJpYXQiOjE1NzgxOTUzNjV9.8CBLsqvjezKEhB1CiGVAdcxavp4d0jcL5CEPkex-mLU'
    }
  })*/