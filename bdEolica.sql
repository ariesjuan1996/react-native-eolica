CREATE TABLE usuario(
	id uuid DEFAULT uuid_generate_v4 () primary key,
	nombre varchar(40) not null,
	correo varchar(72) null ,
	telefono varchar(25) null,
	contrasenia varchar(500) not null,
	estado boolean not null,
	fechaCreacion TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
 CREATE TABLE producto(
	idProducto uuid DEFAULT uuid_generate_v4 () primary key,
        versionProducto NUMERIC(5, 2) not null,
        modelo varchar(40) not null,
        fechaCreacion TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
        serial uuid DEFAULT uuid_generate_v4 () unique,
        usuario varchar(40) not null,
        contrasenia varchar(500) not null,
        estado boolean  default true not null
)
;
CREATE TABLE usuarioproducto(
	idProducto uuid,
	idUsuario uuid,
	nombre varchar(40) not null,
	 estado boolean default true not null,
	fechaCreacion TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
alter table usuarioproducto add constraint fk_idProducto foreign key (idProducto) references producto(idProducto);
alter table usuarioproducto add constraint fk_idUsuario foreign key (idUsuario) references usuario(id);
ALTER TABLE usuarioproducto ADD PRIMARY KEY (idProducto, idUsuario);


CREATE OR REPLACE function  creacionUsuario(p_nombre varchar,p_valor varchar,p_tipo integer,p_contrasenia varchar)
  RETURNS TABLE (
   estado boolean,
   mensaje Text) 
as
$BODY$
DECLARE
countTelefono int:=0;
countCorreo int:=0;
countPemitido int :=1;
msgIdentif Text :='';
BEGIN
  if(p_tipo=1) then
  countTelefono:=(select count(*) from usuario where telefono=p_valor);
  IF (countTelefono>0) THEN
    countPemitido:=0;
    msgIdentif :='Se ha registrado el número de teléfono móvil.Utilice un número teléfono móvil diferente.';
  END IF;
  else
    countCorreo:=(select count(*) from usuario where correo=p_valor);
    IF (countCorreo>0) THEN
     countPemitido:=0;
     msgIdentif:='Se ha registrado el correo.Utilice un correo diferente.';
    END IF;  
  end if;
  if(countPemitido=0) then
     RETURN QUERY  select false,msgIdentif;
   else
     if(p_tipo=1) then
       insert into usuario(nombre,correo,telefono,contrasenia,estado) 
		values(p_nombre,null,p_valor,MD5(p_contrasenia),true);
       RETURN QUERY  select true,'usuario creado con éxito.';
     else
       insert into usuario(nombre,correo,telefono,contrasenia,estado) 
		values(p_nombre,p_valor,null,MD5(p_contrasenia),true); 

        RETURN QUERY  select true,'usuario creado con éxito.';		
     end if;
   end if;

END
$BODY$
LANGUAGE 'plpgsql' ;
CREATE OR REPLACE function  verificarIdenticador(p_valor varchar,p_tipo integer)
  RETURNS TABLE (
   estado boolean,
   mensaje Text) 
as
$BODY$
DECLARE
countTelefono int:=0;
countCorreo int:=0;
BEGIN
  if(p_tipo=1) then
  countTelefono:=(select count(*) from usuario where telefono=p_valor);
  IF (countTelefono>0) THEN
     RETURN QUERY  select false,'El número de teléfono ya esta registrado.Utilice un número teléfono móvil diferente.';
    else
      RETURN QUERY  select true,'Numero telefonico permitido.';
  end if;
  else
    countCorreo:=(select count(*) from usuario where correo=p_valor);
    IF (countCorreo>0) THEN

      RETURN QUERY  select false,'El correo ya esta registrado el correo.Utilice un correo diferente.';
     else
      RETURN QUERY  select true,'correo permitido.';
    END IF;  
  end if;
END
$BODY$
LANGUAGE 'plpgsql' ;

CREATE OR REPLACE function  verificarCredencialesProducto(p_usuario varchar,p_contrasenia varchar)
  RETURNS TABLE (
   estado boolean,
   mensaje text) 
as
$BODY$
DECLARE
  countVerificacionProducto int:=0;
 BEGIN
 
 countVerificacionProducto:=(select count(*) from producto where usuario=p_usuario and contrasenia=md5(p_contrasenia));
 if(countVerificacionProducto>0) then 
    return query select true,'credenciales correcta';
  else
  return query select false,'credenciales incorrectaa';
 end if;
    
END
$BODY$
LANGUAGE 'plpgsql' ;

CREATE OR REPLACE function  accesoSistema(p_valor varchar,p_contrasenia varchar,tipo int)
  RETURNS TABLE (
   estado boolean,
   mensaje text,rid Text,rnombre Text,rcorreo Text,rtelefono Text) 
as
$BODY$
DECLARE
  countVerificacion int:=0;
  countEstadoUsuario int:=0;
  countCredencial int:=0;
 BEGIN
if(tipo=1) then
  countVerificacion:=(select count(*) from usuario as us where   us.nombre=p_valor);
  IF (countVerificacion=0) THEN
    RETURN QUERY select false ,'identificador no existe no existe.',null,null,null,null;
  END IF;
  
  IF (countVerificacion>0) THEN
     countEstadoUsuario:=(select count(*) from usuario as us
                   where    us.nombre=p_valor and us.estado=true);
     if(countEstadoUsuario=0) THEN
	  RETURN QUERY select false ,'usuario inactivo.',null,null,null,null;
     else
	countCredencial:=(select count(*) from usuario as us                   
	        where   us.nombre=p_valor and us.contrasenia=MD5(p_contrasenia));
	if(countCredencial=0) THEN
	  RETURN QUERY select false ,'credenciales incorrecto.',null,null,null,null;
	else
	RETURN QUERY select true,'bienvenido',CAST (us.id AS Text) ,CAST (us.nombre AS Text) ,CAST (us.correo  AS Text),CAST (us.telefono  AS Text) from usuario as us                 
	   where   us.nombre=p_valor   and us.contrasenia=MD5(p_contrasenia);
	end if;
     end if;
    
   end if;
   
  end if; 
  
 if(tipo=2) then
  countVerificacion:=(select count(*) from usuario as us where   us.correo=p_valor);
  IF (countVerificacion=0) THEN
    RETURN QUERY select false ,'identificador no existe no existe.',null,null,null,null;
  END IF;
  
  IF (countVerificacion>0) THEN
     countEstadoUsuario:=(select count(*) from usuario as us
                   where    us.correo=p_valor and us.estado=true);
     if(countEstadoUsuario=0) THEN
	  RETURN QUERY select false ,'usuario inactivo.',null,null,null,null;
     else
	countCredencial:=(select count(*) from usuario as us                   
	        where   us.correo=p_valor and us.contrasenia=MD5(p_contrasenia));
	if(countCredencial=0) THEN
	  RETURN QUERY select false ,'credenciales incorrecto.',null,null,null,null;
	else
	RETURN QUERY select true,'bienvenido',CAST (us.id AS Text) ,CAST (us.nombre AS Text) ,CAST (us.correo  AS Text),CAST (us.telefono  AS Text) from usuario as us                 
	   where   us.correo=p_valor   and us.contrasenia=MD5(p_contrasenia);
	end if;
     end if;
    
   end if;
   
  end if;
if(tipo=3) then
  countVerificacion:=(select count(*) from usuario as us where   us.telefono=p_valor);
  IF (countVerificacion=0) THEN
    RETURN QUERY select false ,'identificador no existe no existe.',null,null,null,null;
  END IF;
  
  IF (countVerificacion>0) THEN
     countEstadoUsuario:=(select count(*) from usuario as us
                   where    us.telefono=p_valor and us.estado=true);
     if(countEstadoUsuario=0) THEN
	  RETURN QUERY select false ,'usuario inactivo.',null,null,null,null;
     else
	countCredencial:=(select count(*) from usuario as us                   
	        where   us.telefono=p_valor and us.contrasenia=MD5(p_contrasenia));
	if(countCredencial=0) THEN
	  RETURN QUERY select false ,'credenciales incorrecto.',null,null,null,null;
	else
	RETURN QUERY select true,'bienvenido',CAST (us.id AS Text) ,CAST (us.nombre AS Text) ,CAST (us.correo  AS Text),CAST (us.telefono  AS Text) from usuario as us                 
	   where   us.telefono=p_valor   and us.contrasenia=MD5(p_contrasenia);
	end if;
     end if;
    
   end if;
   
  end if;
END
$BODY$
LANGUAGE 'plpgsql' ;

CREATE OR REPLACE function  actualizarContrasenia(p_usuario uuid,p_contraseniaActual varchar,p_nuevacontrasenia varchar)
  RETURNS TABLE (
   estado boolean,
   mensaje text) 
as
$BODY$
DECLARE
  countVerificacion int:=0;
BEGIN
  countVerificacion:=(select COUNT(*) from  usuario as us   where us.id=p_usuario and us.contrasenia=MD5(p_contraseniaActual));
  IF (countVerificacion=0) THEN
    RETURN QUERY select false ,'La contraseña actual que has introducido es incorrecta. Vuelve a intentarlo.';
  END IF;
  
  IF (countVerificacion>0) THEN
      IF (p_contraseniaActual=p_nuevacontrasenia) THEN
	RETURN QUERY select false ,'Ingrese una contraseña diferente a la actual.';
      else
       update usuario set contrasenia=MD5(p_nuevacontrasenia) where id=p_usuario and contrasenia=MD5(p_contraseniaActual);
       RETURN QUERY select true ,'usuario actualizado.';
     end if;
    
   end if;

END
$BODY$
LANGUAGE 'plpgsql' ;

CREATE OR REPLACE function  restabler_contrasenia(valor Text,p_nuevacontrasenia varchar,tipoValor int)
  RETURNS TABLE (
   estado boolean,
   mensaje text) 
as
$BODY$
DECLARE
  countVerificacion int:=0;
BEGIN
if(not (tipoValor=1  OR tipoValor=2  OR tipoValor=3)) then
RETURN QUERY select false ,'tipo  invalido.';
else
	if(tipoValor=1) then
	  countVerificacion:=(select COUNT(*) from  usuario as us   where us.nombre=valor);
	  IF (countVerificacion=0) THEN
	    RETURN QUERY select false ,'Credenciales incorrecta.';
	  END IF;
	  
	  IF (countVerificacion>0) THEN
	     update usuario set contrasenia=MD5(p_nuevacontrasenia) where nombre=valor;
		  RETURN QUERY select true ,'usuario actualizado.';
	    
	   end if;
   
	end if;
	if(tipoValor=2) then
	
	  countVerificacion:=(select COUNT(*) from  usuario as us   where us.correo=valor);
	  IF (countVerificacion=0) THEN
	    RETURN QUERY select false ,'Credenciales incorrecta.';
	  END IF;
	  
	  IF (countVerificacion>0) THEN
	     update usuario set contrasenia=MD5(p_nuevacontrasenia) where correo=valor;
		  RETURN QUERY select true ,'usuario actualizado.';
	    
	   end if;  
	end if;
	
	if(tipoValor=3) then
	  countVerificacion:=(select COUNT(*) from  usuario as us   where us.telefono=valor);
	  IF (countVerificacion=0) THEN
	    RETURN QUERY select false ,'Credenciales incorrecta.';
	  END IF;
	  
	  IF (countVerificacion>0) THEN
	     update usuario set contrasenia=MD5(p_nuevacontrasenia) where telefono=valor;
		  RETURN QUERY select true ,'usuario actualizado.';
	    
	   end if;

	end if;

end if;

END
$BODY$
LANGUAGE 'plpgsql' ;

CREATE OR REPLACE function  buscarProducto(pserial uuid)
  RETURNS TABLE (
   id uuid,
   versionproducto numeric,
   modelo character varying,
   serial uuid) 
as
$BODY$
BEGIN

 RETURN QUERY select p.idproducto,p.versionproducto,p.modelo,p.serial from producto as p where p.serial=pserial and p.estado=true;

END
$BODY$
LANGUAGE 'plpgsql' ;

CREATE OR REPLACE function  integrarDispositivo(p_idusuario uuid,p_serial uuid,p_nombre Text,p_usuario varchar,p_contrasenia varchar)
  RETURNS TABLE (
   estado boolean,
   mensaje Text) 
as
$BODY$
 declare 
   verificarUsuario int:=0;
   verificarProducto int:=0;
   verificar int:=0;
   verificarCredenciales int:=0;
   idProductoS uuid;
BEGIN
  verificarUsuario:=(select count(*) from usuario where id=p_idusuario);
  if(verificarUsuario=0) then
    RETURN QUERY select false , 'El usuario no existe o esta inactivo.';
  end  if;
  verificarProducto:=(select count(*) from producto as p  where p.serial=p_serial and p.estado=true);
  if(verificarProducto=0) then
    RETURN QUERY select false , 'La serial del producto no existe o esta inactivo.';
  end  if;
  verificar:=(select count(*) from usuarioproducto up  inner join producto p on p.idproducto=up.idproducto where p.serial=p_serial and up.idusuario=p_idusuario);
  if(verificar=1) then
    RETURN QUERY select false , 'EL producto ya esta registrado.';
  end if;
 
  
  if (verificarUsuario>0 and verificarProducto>0 and verificar=0)then
    verificarCredenciales:=(select count(*) from producto pr where pr.usuario=p_usuario and pr.contrasenia=md5(p_contrasenia) and pr.serial=p_serial);
      if (verificarCredenciales=1) then
       idProductoS:=(select idproducto from producto pr where pr.serial=p_serial);
       insert into usuarioproducto(idusuario,nombre,idproducto) values(p_idusuario,p_nombre,idProductoS);
       
       RETURN QUERY select true,'Producto ha sido asociado con exito.';
      else
       RETURN QUERY select true,'Credenciales del producto  son incorrecta.';
      end if;

  end if;
END
$BODY$
LANGUAGE 'plpgsql' ;

CREATE OR REPLACE function  getProductosUsuarios(p_idusuario uuid)
  RETURNS TABLE (
   nombre character varying,
   fechacreacion timestamp with time zone,
   serial uuid,
   versionproducto numeric,
   modelo character varying) 
as
$BODY$
 declare 
   verificarUsuario int:=0;
BEGIN
  verificarUsuario:=(select count(*) from usuario where id=p_idusuario);
  if(verificarUsuario=0) then
    RETURN QUERY select false , 'El usuario no existe o esta inactivo.';
   else
    return query select up.nombre,up.fechaCreacion,p.serial,p.versionproducto,p.modelo    from usuarioproducto as up inner join producto p on p.idproducto=up.idproducto where up.estado=true and p.estado=true and up.idusuario=p_idusuario; 
  end  if;
  

END
$BODY$

LANGUAGE 'plpgsql' ;
CREATE OR REPLACE function  actualizarUsuarioCampos(p_idusuario uuid,p_campo varchar,p_valor varchar)
  RETURNS TABLE (
   estado boolean,
   mensaje Text) 
as
$BODY$
 declare 
  
   verificarValor   int:=0;
   verificarPaCorreoporId   Text:='';
   verificarIdporCorreo   uuid;
   countValor  int:=0;
   varMensaje Text:='';
BEGIN
  verificarValor:=(select count(*) from usuario us where us.id=p_idusuario and us.estado=true);
  if(verificarValor=0) then
    RETURN QUERY select false , 'El usuario no existe o esta inactivo.';
   else
   
     if(p_campo='CORREO') then
       verificarPaCorreoporId:=(select us.correo from usuario us where us.id=p_idusuario);
       if(verificarPaCorreoporId=null or verificarPaCorreoporId='' ) then 
          countValor:=(select count(*) from usuario us where us.correo=p_valor); 
          if(countValor>0)then
           varMensaje:='El correo  ya esta sociado  ha una cuenta.';
          end if;
       else
         verificarIdporCorreo :=(select us.id from usuario us where us.correo=p_valor);
         if(not verificarIdporCorreo=p_idusuario) then 
           varMensaje:='El correo  ya esta sociado ha una cuenta.';
         end if;
       end if;
       if(varMensaje=null or varMensaje='') then
         update usuario set correo=p_valor where id=p_idusuario;
         return query select  true,'El correo se actualizo con exito.';
       else
         return query select  false,varMensaje;
       end if;
      
      
     end  if;


     if(p_campo='TELEFONO') then
       verificarPaCorreoporId:=(select us.telefono from usuario us where us.id=p_idusuario);
       if(verificarPaCorreoporId=null or verificarPaCorreoporId='' ) then 
          countValor:=(select count(*) from usuario us where us.telefono=p_valor); 
          if(countValor>0)then
           varMensaje:='El telefono  ya esta sociado  ha una cuenta.';
          end if;
       else
         verificarIdporCorreo :=(select us.id from usuario us where us.telefono=p_valor);
         if(not verificarIdporCorreo=p_idusuario) then 
           varMensaje:='El telefono  ya esta sociado  un cuenta.';
         end if;
       end if;
       if(varMensaje=null or varMensaje='') then
         update usuario set telefono=p_valor where id=p_idusuario;
         return query select  true,'El telefono se actualizo con exito.';
       else
         return query select  false,varMensaje;
       end if;
      
      
     end  if;

     if(p_campo='USUARIO') then
       verificarPaCorreoporId:=(select us.nombre from usuario us where us.id=p_idusuario);
       if(verificarPaCorreoporId=null or verificarPaCorreoporId='' ) then 
          countValor:=(select count(*) from usuario us where us.nombre=p_valor); 
          if(countValor>0)then
           varMensaje:='El usuario  ya esta sociado  ha una cuenta.';
          end if;
       else
         verificarIdporCorreo :=(select us.id from usuario us where us.nombre=p_valor);
         if(not verificarIdporCorreo=p_idusuario) then 
           varMensaje:='El usuario  ya esta sociado  un cuenta.';
         end if;
       end if;
       if(varMensaje=null or varMensaje='') then
         update usuario set nombre=p_valor where id=p_idusuario;
         return query select  true,'El usuario se actualizo con exito.';
       else
         return query select  false,varMensaje;
       end if;
      
      
     end  if;
END IF; 
END
$BODY$
LANGUAGE 'plpgsql';


CREATE OR REPLACE function  actualizarUsuario(p_idusuario uuid,p_nombre varchar,p_correo varchar,p_telefono varchar)
  RETURNS TABLE (
   estado boolean,
   mensaje Text) 
as
$BODY$
 declare 
   verificarUsuario  int:=0;
   verificarCorreo   Text:='';
   countCorreo  int:=0;
   verificarTelefono Text:='';
   countTelefono  int:=0;
   varMensaje Text:='';
   varComa Text:='';
BEGIN
  verificarUsuario:=(select count(*) from usuario us where us.id=p_idusuario and us.estado=true);
  if(verificarUsuario=0) then
    RETURN QUERY select false , 'El usuario no existe o esta inactivo.';
   else
     verificarCorreo:=(select us.correo from usuario us where us.id=p_idusuario);
      if(verificarCorreo=null or verificarCorreo='' ) then 
       countCorreo:=(select count(*) from usuario us where us.correo=p_correo); 
        if(countCorreo>0)then
         varMensaje:='El correo  ya esta sociado  un cuenta';
        end if;
         
      end if;
      verificarTelefono:=(select us.telefono from usuario us where us.id=p_idusuario);
      if(verificarTelefono=null or verificarTelefono='' ) then 
       countTelefono:=(select count(*) from usuario us where us.telefono=p_telefono); 
       if(countTelefono>0)then
          if(not varMensaje='') then
            varComa=' y ';
          end if;
         varMensaje:=concat(varMensaje,varComa,'El telefono ya esta asociado un cuenta.');
       end if;
      end if;
      if(countCorreo=0 and countTelefono=0) then
        update usuario set nombre=p_nombre,correo=p_correo,telefono=p_telefono where id=p_idusuario;
        return query select true,'El usuario se actualizo.'; 
      else
        return query select false,varMensaje; 
      end  if;
    end  if;
 
END
$BODY$
LANGUAGE 'plpgsql';

CREATE OR REPLACE function  verificarUsuario(p_valor Text,p_tipo int)
  RETURNS TABLE (
   estado boolean,
   mensaje Text,
   idUsuario Text) 
as
$BODY$
 declare 
   existeValor int :=0;
   valor Text :='';
BEGIN
  if (p_tipo=1) then
     existeValor:=(select count(*) from usuario where nombre=p_valor);
     if(existeValor=0) then 
        return query select false,'El nombre de usuario no existe.',null; 
      else
      valor:=(select id from usuario where nombre=p_valor);
       return query select true,'El nombre de usuario  existe.',valor; 
     end if;
  end if;
  if (p_tipo=2) then
    existeValor:=(select count(*) from usuario where correo=p_valor);
     if(existeValor=0) then
        return query select false,'El correo del no existe.',null; 
      else
        valor:=(select id from usuario where correo=p_valor); 
        return query select true,'El correo del usuario  existe.',valor; 
     end if;
  end if;
  if (p_tipo=3) then
   existeValor:=(select count(*) from usuario where telefono=p_valor);
    if(existeValor=0) then
        return query select false,'El telefono del ya esta no esta registrado con un usuario.',null;  
      else
        valor:=(select id from usuario where telefono=p_valor); 
        return query select true,'El telefono del ya existe.',valor; 
     end if;
  end if;
END
$BODY$
LANGUAGE 'plpgsql';

select *from usuario;
select *from  buscarUsuario('c766d8d1-b3e3-48b2-a271-72cf795be02c');
DROP FUNCTION buscarusuario(uuid);
CREATE OR REPLACE function  buscarUsuario(pIdUsuario uuid)
  RETURNS TABLE (
   id uuid,
   nombre  character varying,
   correo character varying,
   telefono  character varying,
  estado boolean) 
as
$BODY$
BEGIN

 RETURN QUERY select us.id,us.nombre,us.correo,us.telefono,us.estado  from usuario as us where us.id=pIdUsuario and us.estado=true;

END
$BODY$
LANGUAGE 'plpgsql' ;