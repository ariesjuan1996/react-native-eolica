/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName,hostApi as hostApi} from './app.json';


AppRegistry.registerComponent(appName, () => App);