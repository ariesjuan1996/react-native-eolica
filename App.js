import React, { Component } from 'react';
import IniciarSession from "./src/acceso/login/Login";
import { Root } from "native-base";
import {AsyncStorage, Alert} from 'react-native';
import MenuPrincipal from "./src/menu";
import {HTTP} from './axios';
import PruebaChar from './src/pruebaChar';
import LogoLoader from './src/logoLoader';
import servi from './src/services/mainservice';


export default class App extends Component {
   constructor(props){
    super(props);
    this.state={
      tokenSession :null ,
      estadoLoader:true
    }
    this.actualizarSession=this.actualizarSession.bind(this);
    this.refresh=this.refresh.bind(this);
  }

 async componentDidMount(){
   setTimeout((e)=>{
    this.setState({estadoLoader:!this.state.estadoLoader});
   },4000);
  this.refresh();
}
  async actualizarSession(){
    let valor=await AsyncStorage.getItem('tokenUsuario');
    await this.setState({
      tokenSession :valor
    });
  }
  async refresh(){
    try {
      const Refresh= await HTTP('GET',`verificarSession`,{ });
      const {data}=Refresh;
      if(data.refrescar){
        let valor=await AsyncStorage.getItem('tokenUsuario');
        await this.setState({
          tokenSession :valor
        });
      }else{
        Alert.alert("false");
      }

      
      
    } catch (error) {
      
    }

  }
  render() {
     
     // ?  <LogoLoader/> :
    if(this.state.estadoLoader){
      return (<LogoLoader/> );
      
    }else{
      if(this.state.tokenSession!=null){
        return (
          <Root>
            <MenuPrincipal cerrarSession={this.actualizarSession.bind(this)}/>
          </Root>
          );
        }else{
        return (
          <Root>
            <IniciarSession actualizarSession={this.actualizarSession.bind(this)}/>
          </Root>
          );
      }
    }



  }
}