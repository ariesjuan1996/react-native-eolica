'use strict';

import React, { Component } from 'react';

import {
  StyleSheet
} from 'react-native';
import {HTTP} from '../axios';
import QRCodeScanner from 'react-native-qrcode-scanner';

export default class ScanScreen extends Component {
  constructor(props){
    super(props);
    this.state={
      codigoScaner:null
    } 
  }
  componentDidMount(){
   //this.pruebaVerificar();
  } 
  async pruebaVerificar(){
    //codigoScaner
    await this.setState({
      codigoScaner:"114aa058-4f42-42c1-90ab-4fb5c3adf69e"
    });
    this.verificarProducto();
  }
  async verificarProducto(){
   try {
   let productoIdentificacion=await HTTP('GET',`seguridad/verificarProducto?serial=${this.state.codigoScaner}`,{});
   const {data}=productoIdentificacion;
   if(data.estado){
      this.props.identificarProducto(data.data);
  }else{
      this.props.identificarProducto(null);
  }
  } catch (error) {
    this.props.identificarProducto("undefined");
   }   
   

}
  async onSuccess(e) {
   /**
    * 3 posibles valores 
    * 1 - producto ha sido identificado
    * 2- no se pudo idenficar producto
    * 3- undefined  error de conexion error cath
    */
    await this.setState({
      codigoScaner:e.data
    });
   await this.verificarProducto();
  }
  atras(){
    this.props.cerrar( e.data);
  }

  render() {
    return (
    <>
       <QRCodeScanner
        onRead={this.onSuccess.bind(this)}
        /*topContent={
       
        }*/
        /*
        bottomContent={
          <TouchableOpacity style={styles.buttonTouchable}>
            <Text style={styles.buttonText}>OK. Got it!</Text>
          </TouchableOpacity>
        }*/
      />
      </>
    );
  }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});

