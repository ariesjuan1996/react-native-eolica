import React, { Component } from "react";
import {Header, Text,Container,ListItem, Right,Title,Icon, Left } from "native-base";
import { Button } from "react-native-paper";
import { Dimensions } from 'react-native';
const screenWidth = Dimensions.get('window').width;
export default class CardItemButton extends Component {
  constructor(props){
   super(props);
   this.state={
     selection:0,
   }

  }
  atras(val){
      this.props.atras(0);
  }
  listo(){
    this.props.listo();
  }
  render() {
    return (
      <Header style={{backgroundColor:"#2F3AA6",borderBottomColor:"#2F3AA6",borderBottomWidth:0.5,height : 50,width:"100%"}}>        
        <Text style={{width:40,marginTop:10}}  onPress={this.atras.bind(this)}><Icon vertical name="ios-arrow-back" style={{color:"#FFFFFF"}} onPress={this.atras.bind(this)}/></Text>
        <Text style={{textAlign:"center",marginTop:12,fontSize:18,width:screenWidth-90,color:"#FFFFFF"}}>{this.props.titulo}</Text>
        {this.props.isListo ?         
        <Right>
          <Text   style={{marginTop:8,width:50,color:"#fff",fontSize:18}}
           onPress={this.listo.bind(this)}>listo</Text>
         </Right> : null
         }

     
    
       </Header>
 
     

    );
  }
}