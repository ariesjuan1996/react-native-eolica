import React, { Component } from 'react';
import {Content, Card, CardItem,Button, Text,Container,Form,Item,Input,Toast} from "native-base";
import { Alert } from 'react-native';
import Header from '../header';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IdentificacionScaner from '../identificacion';
import {HTTP} from '../../axios';
export default class InstalarDispositivo extends Component {
  constructor(props){
    super(props);
    this.state={
      swipeableModal:false,
      escanerProducto : false,
      usuario:null,
      contrasenia:null,
      serial:null,
      nombreProducto:null,
      dataProducto:{}
      
    }
  }
  nuevoDispositivo(){
    this.setState({swipeableModal :!this.state.swipeableModal});
  }
  confirmarNombre(){
  }
  atras(){
    if(this.state.escanerProducto){
      this.setState({
        escanerProducto :false
      });
    }else{
      this.props.atras();
    }
  }
  async changeValor(target){
    await this.setState(target);
  }
  async identificarProducto(dat){
    this.setState({escanerProducto:!this.state.escanerProducto});
    if(dat=="undefined"){
      Toast.show({
        text: "Error de verificación.",
        position: "bottom",
        duration: 5000
      }); 
      return ;
    }
    if(dat==null){
      Toast.show({
        text: "Producto no encontrado.",
        position: "bottom",
        duration: 5000
      }); 
      return ;
    }
    await this.setState({
      dataProducto:dat
    });
    await this.setState({
      serial:dat.serial
    });
  }
  async instalarDispositivo(){
   if(!(!this.state.contrasenia || !this.state.usuario || !this.state.serial || !this.state.nombreProducto)){
     try {
      const valor=await HTTP('POST',`producto/instalarDispositivo`,{
        "serial":this.state.serial,
        "usuario":this.state.usuario,
        "contrasenia":this.state.usuario,
        "nombreProducto":this.state.nombreProducto  
      });
      const {data}=valor;

      if(data.estado){
        Alert.alert(
          'dispositivos',
          data.mensaje,
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
        this.props.atras();
      }else{
        Toast.show({
          text: data.mensaje,
          position: "bottom",
          duration: 5000
        });
      }
     /* if(data.estado){
        /*Alert.alert(
          'dispositivos',
          data.mensaje,
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
      }else{
      Toast.show({
          text: "no existe",
          position: "bottom",
          duration: 5000
        });
      }*/

  
     } catch (error) {
      Alert.alert(
        'Alert Title',
        JSON.stringify(error),
        [
          {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      );
     }
   }
  }
  render() {
    return ( 
      < >
      
      <Header atras={this.atras.bind(this)} titulo={this.state.escanerProducto ? "identificar producto" : "Instalar de nuevo dispositivo"}/>
        <Container>
          {this.state.escanerProducto ? <IdentificacionScaner atras={this.atras.bind(this)} identificarProducto={this.identificarProducto.bind(this)}/>  :
        <Card>
          <CardItem>
            <Content>
              <Form>
                <Item error={this.state.nombreProducto=='' ? !this.state.nombreProducto : null}>
                   <IconFontAwesome
                     name={"product-hunt"}
                     color="#000000"
                     size={25}
                   />
                   <Input placeholder='Nombre del producto' value={this.state.nombreProducto} onChangeText={(nombreProducto) => this.changeValor({nombreProducto})}/>
                 </Item>
                 <Item error={this.state.serial=='' ? !this.state.serial : null}>
                   <Input 
                    style={{fontSize:12}}
                    placeholder='Codigo de producto'
                    onChangeText={(idProducto) => this.changeValor({idProducto})} value={this.state.serial}/>
                   <IconAntDesign
                     name={"qrcode"}
                     color="#000000"
                     size={25}
                     onPress={(e)=>{this.setState({escanerProducto:!this.state.escanerProducto})}}
                   />
                 </Item>
                 <Item error={this.state.usuario=='' ? !this.state.usuario : null}>
                  <IconAntDesign
                     name={"user"}
                     color="#000000"
                     size={25}
                   />
                   <Input placeholder='Usuario'  onChangeText={(usuario) => this.changeValor({usuario})} value={this.state.usuario}/>
                 </Item>
                 <Item error={this.state.contrasenia=='' ? !this.state.contrasenia : null}>
                 <IconMaterialCommunityIcons
                     name={"textbox-password"}
                     color="#000000"
                     size={25}
                   />
                   <Input secureTextEntry placeholder='Contraseña' 
                    value={this.state.contrasenia}
                    onChangeText={(contrasenia) => this.changeValor({contrasenia})}/>
                 </Item>
                <Button style={{marginTop:30}} disabled={!this.state.contrasenia || !this.state.usuario || !this.state.serial || !this.state.nombreProducto}><Text style={{textAlign:"center",width:"100%"}} onPress={this.instalarDispositivo.bind(this)}>Instalar</Text></Button>
               </Form>
             </Content>
           </CardItem>
           </Card>}
      </Container>
    </>

    );
  }
}