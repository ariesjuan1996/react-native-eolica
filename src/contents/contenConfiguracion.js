import React, { Component } from 'react';
import { Container} from 'native-base';
import ActualizarContrasenia from '../acceso/actualizarContrasenia';
import OpcionesConfiguracion from './opcionesConfiguracion';

export default class contentConfiguracion extends Component {
  constructor(props){
    super(props);
    this.state={
      opcionSeleccionada:1
    };
    this.atras=this.atras.bind(this);
  }
  atras(){
    this.setState({
      opcionSeleccionada:1
    });
  }
  cambioOpcion(val){
    this.props.cambioOpcion(val);
  }
  async cerrarSession(){
   await  this.props.cerrarSession();
  }
  render() {
    return (   
    <Container>  
    <OpcionesConfiguracion cambioOpcion={this.cambioOpcion.bind(this)} cerrarSession={this.cerrarSession.bind(this)}/>
    
    </Container> );
  }
}