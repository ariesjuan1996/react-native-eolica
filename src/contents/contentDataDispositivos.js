import React, { Component } from 'react';
import { Container,Content, Card, CardItem, Text, Body } from "native-base";
import { Col, Grid } from 'react-native-easy-grid';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IcoEntypo from 'react-native-vector-icons/Entypo';
import {  Image } from 'react-native';
import { Button, Segment } from 'native-base';
import {HTTP} from '../../axios';
import io from "socket.io-client";
import PruebaChar from '../pruebaChar';
export default class CardPrincipal extends Component {
  constructor(props){
    super(props);
    this.state={
      cont:0,
      segmentActive:0,
      dispostivosInstalados:[],
    }
    this.socket = io("http://192.168.0.103:3001");
  }
  componentDidMount(){
    this.getDataProductosInstalados().then(()=>{
       this.dataSensores();
   });
 

}
async dataSensores(){
    this.state.dispostivosInstalados.map((e)=>{
      this.socket.on("getDataSensensor"+e.serial, data => {
         this.setState(data);
       });
    });
  }
  async getDataProductosInstalados(){
    const dispositivosInstalados=await HTTP('GET',`producto/dispostivosUsuarios`,{});
    const {data}=dispositivosInstalados;
    try {
      if(data.estado){
        await this.setState({
          dispostivosInstalados:data.data
        });
      }else{
        await this.setState({
          dispostivosInstalados:[]
        });
      }
    } catch (error) {
      this.setState({
        dispostivosInstalados:[]
      });
    }  
    

  }
  render() {
    const datosActual=( <Content padder>
      <Card>
        {this.state.dispostivosInstalados.map(element => {
           return < >
            <CardItem header bordered key={element.id}>
            <Text>{element.nombre}</Text>
            </CardItem>
            <CardItem bordered>
              <Body>
              <Grid>
                <Col >
                  <Text style={{width:"100%",fontSize:20}}>Viento-anemometro</Text>
                  <IconFontAwesome5  
                    name={"wind"}
                    color="#000000"
                    size={25} />
                </Col>
                <Col >
                  <Text style={{width:"100%",fontSize:20}}>{(this.state[element.serial]) ? (this.state[element.serial].anemometro+"km/h"):"esperando..."}</Text>
                </Col>
              </Grid>
              <Grid>
                <Col >
                  <Text style={{width:"100%",fontSize:20}}>Tacometro</Text>
                  <IconFontAwesome5  
                    name={"tachometer-alt"}
                    color="#000000"
                    size={25} />
                </Col>
                <Col >
                  <Text style={{width:"100%",fontSize:20}}>{(this.state[element.serial]) ? (this.state[element.serial].tacometro+"RPM"):"esperando..."}</Text>
                </Col>
              </Grid>

              <Grid>
                <Col >
                  <Text style={{width:"100%",fontSize:20}}>Temperatura</Text>
                  <IconFontAwesome5  
                    name={"temperature-low"}
                    color="#000000"
                    size={25} />
                </Col>
                <Col >
                  <Text style={{width:"100%",fontSize:20}}>{(this.state[element.serial]) ? (this.state[element.serial].temperatura+"° C"):"esperando..."}
                  </Text>
                </Col>
              </Grid>
              
              <Grid>
                <Col >
                  <Text style={{width:"100%",fontSize:20}}>voltimetro</Text>
                  <IcoEntypo
                    name={"battery"}
                    color="#000000"
                    size={25}
                  />
                </Col>
                <Col > 
                  <Text style={{width:"100%",fontSize:20}}>{(this.state[element.serial]) ? (this.state[element.serial].voltimetro+"V"):"esperando..."}</Text>
                </Col>
              </Grid>
              <Grid>
                <Col >
                  <Text style={{width:"100%",fontSize:20}}>amperimetro</Text>
                  <Image
                    style={{width: 25, height: 25}}
                    source={require('../../imagenes/amperimetro.png')}
                  />
                </Col>
                <Col >
                  <Text style={{width:"100%",fontSize:20}}>{(this.state[element.serial]) ? (this.state[element.serial].amperimetro+"A"):"esperando..."}</Text>
                </Col>
              </Grid>
              </Body>
            </CardItem>

           </>          
        })}

      </Card>
    </Content>
 );
 const datosReportes=( 
 <Content padder>
  <PruebaChar/>
</Content>
);
    return (
            <Container>
              <Segment >
                <Button first active={this.state.segmentActive==0 ? true : false} onPress={()=>{
                  this.setState({segmentActive:0});
                }}><Text>Monitoreo actual</Text></Button>
                <Button last active={this.state.segmentActive==1 ? true : false} onPress={()=>{
                  this.setState({segmentActive:1});
                }}><Text>Reṕorte</Text></Button>
              </Segment>
                {this.state.segmentActive==0  ? datosActual : datosReportes}
             </Container>

    );
  }
}