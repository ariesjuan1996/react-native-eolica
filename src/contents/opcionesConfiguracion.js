import React, { Component } from 'react';
import { View, Card, CardItem, Thumbnail, Text, Left, Body,List,ListItem } from 'native-base';
import {AsyncStorage,Alert} from 'react-native';
const cards = [
    {
      text: 'Juan Ramírez Sánchez',
      name: 'One',
      image: {uri :'https://cdn.icon-icons.com/icons2/827/PNG/512/user_icon-icons.com_66546.png'},
    },
  ];
  
export default class opcionesConfiguracion extends Component {
  constructor(props){
    super(props);
    this.state={
      opcionSeleccionada:1,
      defaultAnimationModal:false
    };
    this.atras=this.atras.bind(this);
  }
  atras(){

  }
  cambioOpcion(val){
   this.props.cambioOpcion(val);   
  }
  cerrarSession(){
    Alert.alert(
      'Sessión',
      '¿Deseas realmente cerrar session ?',
      [
        {
          text: 'Cancelar',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Confirmar', onPress: async() =>{
         await AsyncStorage.removeItem('tokenUsuario');
         await AsyncStorage.removeItem('datos');  
         await this.setState({ defaultAnimationModal: false });
         await this.props.cerrarSession();
        }},
      ],
      {cancelable: false},
    );
    
    this.setState({
      defaultAnimationModal: true,
    });
  }
  async confirmarCierre(){

  }
  render() {
    return (
    <View>

          
          <Card style={{ elevation: 1}}>
              <CardItem cardBody style={{width:"100%",paddingBottom:50}}>
                  <List style={{width:"100%"}}>
                      <ListItem itemHeader >
                        <Text>Configuracíon</Text>
                      </ListItem>
                      <ListItem  onPress={this.cambioOpcion.bind(this,"ACTUALIZAR_INFO")}>
                        <Text>Información General</Text>
                      </ListItem>
                      <ListItem  onPress={this.cambioOpcion.bind(this,"ACTUALIZAR")}>
                        <Text>actualizar contraseña</Text>
                      </ListItem>
                      <ListItem  onPress={this.cambioOpcion.bind(this,"NOTIFICACION")}>
                        <Text>notificaciones</Text>
                      </ListItem>
                      <ListItem  onPress={this.cerrarSession.bind(this,4)}>
                        <Text>cerrar session</Text>
                      </ListItem>
                  </List>
              </CardItem>
          </Card>
      

  </View>);
  }
}
