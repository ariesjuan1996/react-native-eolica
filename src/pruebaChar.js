import React, { Component } from 'react';
import {  View ,ScrollView,StyleSheet,Text} from 'react-native';

  import PureChart from 'react-native-pure-chart';
  import { Dimensions } from 'react-native';
  const screenWidth = Dimensions.get('window').width;
  import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from "react-native-chart-kit";
  import { Col, Row, Grid } from "react-native-easy-grid";
export default class App extends Component {
  state = {
    pieData: [{label: '사람', value: 110, color: 'red'}, {label: '동물', value: 140, color: 'green'} ],
    customBackgroundModal: false,
    defaultAnimationModal: false,
    swipeableModal: false,
    scaleAnimationModal: false,
    slideAnimationModal: false,
    bottomModalAndTitle: false,
    bottomModal: false,
  };

render() {
  return (
    <View>
      <Text style={{width:"100%",textAlign:"center",fontSize:22}}></Text>
     
      <BarChart
        data={{
          labels: ['January', 'February', 'March'],
          datasets: [
            {
              data: [20, 45, 28],
            },
          ],
        }}
        width={screenWidth}
        height={220}
        yAxisLabel={'v'}
        chartConfig={{
          backgroundColor: '#FFFFFF',
          backgroundGradientFrom: '#3B78E7',
          backgroundGradientTo: '#7E4F4F',
          decimalPlaces: 5, // optional, defaults to 2dp
          color: (opacity =0.1) => `rgba(255, 255, 255,0.5)`,
          style: {
            borderRadius: 25,
            width:"100%"
          }
        }}
      />
    </View>



    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    width:"100%"
  },
  chart: {
    flex: 1
  }
});
