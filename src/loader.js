import React, { Component } from 'react';
import {Content,Container,Spinner } from 'native-base';

/** */
export default class loader extends Component {
  constructor(props){
    super(props);

  }
  render() {
    return (
        <Container style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: "100%",
            height: "100%",
            }}>
            <Content style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: "100%",
                height: "100%",
            }} >
            <Spinner style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: 600,
                height: 400,
            }} color='blue' />
            </Content>
        </Container>      
    );
  }
}
