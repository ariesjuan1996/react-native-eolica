import React, { Component } from 'react';
import CardPrincipal from './CardPrincipal'
import Login from './login/Login';
//import Registrar from './Registrar';
import Mainservice from '../services/mainservice';
import LoaderView from '../loader';
import TipoCuenta from './registro/tipoCuenta';

export default class Pincipal extends Component {
  state = { opcionSeleccionada: 0,loader : true};
  constructor(props){
    super(props);

   //Mainservice.load(e=>{ this.setState({loader : false})},3000);
    this.actualizarSession=this.actualizarSession.bind(this);
  }
  componentDidMount(){

  }
  selectOpcion(val){
    this.setState({
      opcionSeleccionada:val
  });
  }
  actualizarSession(){
  
    this.props.actualizarSession();
  }
  render() {
    if(this.state.opcionSeleccionada == 0){
      return (
        <>
        {this.state.loader ? <LoaderView actualizarloader={this.state.loader} tiempo={5000}/>
          :<CardPrincipal selectOpcion={this.selectOpcion.bind(this)}/>       
          
        }
       
        </>
      );
      }
      if(this.state.opcionSeleccionada == 1){
        return (
          <Login selectOpcion={this.selectOpcion.bind(this)} actualizarSession={this.actualizarSession.bind(this)}/>
        );

      }
      if(this.state.opcionSeleccionada == 2){
        return (
          <TipoCuenta selectOpcion={this.selectOpcion.bind(this)} actualizarSession={this.actualizarSession.bind(this)}/>
        );
      }
      return (
        <CardPrincipal selectOpcion={this.selectOpcion.bind(this)}/>
      );
  }
}