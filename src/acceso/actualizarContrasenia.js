import React, { Component } from 'react';
import {Content,Card,CardItem,Container
,Item,Input ,Toast } from 'native-base';
import {Alert} from 'react-native';
import {HTTP} from '../../axios';
import ComponentHeader from '../header';
import IconFeather from 'react-native-vector-icons/Feather';
import Spinner from 'react-native-loading-spinner-overlay';
export default class actualizarContrasenia extends Component {
  constructor(props){
    super(props);
    this.state = {
      spinner:false,
      eyeIconCon:false,
      eyeIconConNueva:false,
      eyeIconConConfirma:false,
      disabledControl:true,
      contraseniaActual :null,
      contraseniaNueva :null,
      contraseniaConfirmacion :null,
      errorForm :{
        errorContActual:null,
        errorContNueva:null,
        errorContConfirmacion:null
      }
    }
    this.changeContrasenia=this.changeContrasenia.bind(this);

  }
  atras(val){
    this.props.atras(val);   
  }
  async actualizar(){
    this.setState({contraseniaActual: this.state.contraseniaActual==null ? "" : this.state.contraseniaActual });
    this.setState({contraseniaNueva: this.state.contraseniaNueva==null ? "" : this.state.contraseniaNueva });
    this.setState({contraseniaConfirmacion: this.state.contraseniaConfirmacion==null ? "" : this.state.contraseniaConfirmacion });
    if(!this.state.contraseniaActual || !this.state.contraseniaNueva || !this.state.contraseniaConfirmacion ){
      Toast.show({
        text: "Asegúrate de que rellenar todos los campos.",
        position: "bottom",
        duration: 5000
      });
      return false;
    }
    if(this.state.contraseniaNueva==this.state.contraseniaConfirmacion){
        if(this.state.contraseniaNueva.length< 6 || this.state.contraseniaConfirmacion.length< 6){
          Toast.show({
            text: "Crea una contraseña que tenga al menos 6 caracteres.",
            position: "bottom",
            duration: 5000
          });      
          return false;
        }
  
    }else{
        Toast.show({
          text: "Asegúrate de que ambas contraseñas coincidan.",
          position: "bottom",
          duration: 5000
        });
        return false;
    }      
    
    try {
      this.setState({loader : true});
      const valor=await HTTP('PUT',`seguridad/actualizarContrasenia`,{
	      "contraseniaActual":this.state.contraseniaActual,
	      "contrasenia":this.state.contraseniaNueva,
	      "contraseniaVerificacion":this.state.contraseniaConfirmacion
      });
      const {data}=valor;
      this.setState({loader : false});
      if(data.estado){
        Alert.alert(
          'actualización de contraseña',
          data.mensaje
        );
        this.props.atras();
      }else{
        Toast.show({
          text: data.mensaje,
          position: "bottom",
          duration: 5000
        }); 
      }
          
    } catch (error) {
      this.setState({loader : false});
      Toast.show({
        text: JSON.stringify(error),
        position: "bottom",
        duration: 5000
      });  
    }
   

  }
  async changeContrasenia(target){
    var keys = await Object.keys(target);
    await this.setState(target);
    
   if((this.state.contraseniaActual=="" || this.state.contraseniaActual==null ) || (this.state.contraseniaNueva=="" || this.state.contraseniaNueva==null ) || ( this.state.contraseniaConfirmacion==""|| this.state.contraseniaConfirmacion==null) ){
    await  this.setState({
      disabledControl : true
    });
   }else{
    await this.setState({
      disabledControl : false
    });
   }
  }

render() {
  return (
    <Container>
       <ComponentHeader titulo="Actualizar contraseña"  atras={this.atras.bind(this)} listo={this.actualizar.bind(this)} isListo={true} />
        <Card padder style={{height:'100%'}}>
        <Spinner
          visible={this.state.loader}
          textContent={'Loading...'}
          textStyle={{
            color: '#FFF'
          }}
        />
            <CardItem>
              <Content>
                <Item  inlineLabel last error={this.state.contraseniaActual=='' ? !this.state.contraseniaActual : null}> 
                  <Input   secureTextEntry={!this.state.eyeIconCon} placeholder='contraseña actual' value={this.state.contraseniaActual} onChangeText={(contraseniaActual)=>{this.changeContrasenia({contraseniaActual})} }/>
                    <IconFeather
                      name={this.state.eyeIconCon ? "eye-off"   :"eye"}
                      color="#000000"
                      size={25}
                      style={{right:0}}
                      onPress={()=>{this.setState({eyeIconCon:!this.state.eyeIconCon})}}
                    />  
                </Item>
                <Item  inlineLabel last error={this.state.contraseniaNueva=='' ? !this.state.contraseniaNueva : null}> 
                  <Input   secureTextEntry={!this.state.eyeIconConNueva} placeholder='nueva contraseña' value={this.state.contraseniaNueva}   onChangeText={(contraseniaNueva)=>{this.changeContrasenia({contraseniaNueva})} }/>
                    <IconFeather  
                      name={this.state.eyeIconConNueva ? "eye-off"   :"eye"}
                      color="#000000"
                      size={25}
                      style={{right:0}}
                      onPress={()=>{this.setState({eyeIconConNueva:!this.state.eyeIconConNueva})}}
                    />  
                </Item>                
                <Item  inlineLabel last error={(this.state.contraseniaConfirmacion=='' ? !this.state.contraseniaConfirmacion : null)}> 
                  <Input   secureTextEntry={!this.state.eyeIconConNueva} placeholder='contraseña de confirmación' value={this.state.contraseniaConfirmacion}    onChangeText={(contraseniaConfirmacion)=>{this.changeContrasenia({contraseniaConfirmacion})} }/>
                    <IconFeather
                      name={this.state.eyeIconConConfirma ? "eye-off"   :"eye"}
                      color="#000000"
                      size={25}
                      style={{right:0}}
                      onPress={()=>{this.setState({eyeIconConConfirma:!this.state.eyeIconConConfirma})}}
                    />  
                </Item>  
              </Content>
            </CardItem>

            <Content  />
          </Card>
      </Container>
    );
  }
}
