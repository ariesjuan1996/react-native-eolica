import React, { Component } from 'react';
import { Container, Content, Item, Input, Toast,Text,Card ,CardItem,Body,Button} from 'native-base';
import {AsyncStorage,Alert} from 'react-native';
import IconFeather from 'react-native-vector-icons/Feather';
import { Col, Grid } from 'react-native-easy-grid';
import SelectCodigoPais from '../seleccionarCodigoPais';
import {HTTP} from '../../../axios';
import RegistrarUsuariooActualizacion from '../registro/correoOtelefono';

export default class login extends Component {
  constructor(props){
    super(props);
    this.state = {
      hover: false,
      registrarUsuario:true,
      iniciarSession:true,
      sessionCorreoOusuario:true,
      openSelectPais:false,
      usuario:null,
      contrasenia:null,
      disabledControl:false,
      selectedPais:{
        "dial_code":51
      },
      eyeIconCon:false
    }
  }
  componentDidMount(){
  }
  selectOpcion(val){
    this.props.selectOpcion(val);

  }
  atras(val){
    this.props.selectOpcion(val);   
  }
  async login(){
    if(!this.state.usuario || !this.state.contrasenia ){
      Toast.show({
        text: "Rellenar todos los campos son requeridos.",
        position: "bottom",
        duration: 5000
      })
      return false;
    }
    let tipoCuenta=1;
    if(this.state.sessionCorreoOusuario){
      var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      var validacionCorreo=this.state.usuario.match(reg);
      /**validacion de telefono */
      if(validacionCorreo){
        tipoCuenta=2;
      }
    }
   
    try {
      let usuario=this.state.usuario;
      if(!this.state.sessionCorreoOusuario){
        usuario=this.state.selectedPais.dial_code+"-"+this.state.usuario;
      }
      const responseLogueo=await HTTP('POST',`seguridad/logueo`,{
        "valorTipo":usuario,
        "contrasenia":this.state.contrasenia,
        "tipo":this.state.sessionCorreoOusuario ? tipoCuenta : 3
      });

      const {data}=responseLogueo;   
     
      if(data.estado){    
        let datos=data.data;  
        await AsyncStorage.setItem('tokenUsuario',datos.token);
        await AsyncStorage.setItem('datos',JSON.stringify(datos));  

        await this.props.actualizarSession();
      
      }else{
        Toast.show({
          text: "Usuario y contraseña incorrecto.",
          position: "bottom",
          duration: 5000
        });    
      }    
    } catch (error) {
      
      Toast.show({
        text: "error" +JSON.stringify(error),
        position: "bottom",
        duration: 5000
      });
    }

  }
  async changeValor(val){
    await this.setState(val);
    
    var keys = await Object.keys(val);
    await this.setState(val);
    if(keys=="usuario"){
      await this.setState({
        errorUsuario :!this.state.errorUsuario
      });
    }
    if(keys=="contrasenia"){
      await this.setState({
        errorContrasenia :!this.state.errorContrasenia
      });
    }
    if((this.state.usuario=="" || this.state.usuario==null ) 
    || (this.state.contrasenia=="" || this.state.contrasenia==null ) 
     ){
      
      await  this.setState({
        disabledControl : true
      });

     }else{
      await this.setState({
        disabledControl : false
      });
     }
  }
  atras(val){
    this.setState({
      iniciarSession:true,
    }); 
  }  
  handleChange({target}){
    const {name ,value}=target;
    this.setState({[name]:value});
  }
  toggleHover() {
    this.setState({hover: !this.state.hover});
  
  }
  cambiarEstadoModal() {
    this.setState({
      openSelectPais:!this.state.openSelectPais,
    });

  }
  valorSeleccionado(val){
    this.setState({
      selectedPais:val
    });
  }
  iniciarSession(){
    this.props.actualizarSession();
  }
  render() {

   
  return (
<>
{this.state.iniciarSession ?
    <>
      {!this.state.openSelectPais ?
      <Container>
        <Content padder >
         <Card style={{       
           flexGrow:1,
           width:"100%",
           alignItems: 'center',
           justifyContent:'center',}}>
          <CardItem bordered>
          <Body>
            <Text style={{color :"rgb(30, 138, 206)",fontSize:24,textAlign:"center",width:"100%"}}>INICIAR SESSIÓN</Text>
            <Item  onPress={this.cambiarEstadoModal.bind(this)} style={{marginBottom:15}}>
               {!this.state.sessionCorreoOusuario ? <>
                <Text style={{width:35,fontSize:12,textAlign:"center"}}>{this.state.selectedPais ? ("+"+this.state.selectedPais.dial_code)  : ""}</Text>
                <IconFeather
                  name={"arrow-down"}
                  color="#000000"
                  size={25}
                  style={{right:0}}
                  onPress={this.cambiarEstadoModal.bind(this)}
                />  
               </> : null}
                    
                <Input placeholder={this.state.sessionCorreoOusuario ? 'Ingrese usuario o correo electronico' :'Ingrese número telefonico'} value={this.state.usuario} onChangeText={(usuario) => this.changeValor({usuario})}/>
              </Item>
               
              <Item >
                <Input  secureTextEntry={!this.state.eyeIconCon} placeholder='Ingrese contraseña' value={this.state.contrasenia}  onChangeText={(contrasenia) => this.changeValor({contrasenia})}/>
                <IconFeather
                  name={this.state.eyeIconCon ? "eye-off"   :"eye"}
                  color="#000000"
                  size={25}
                  style={{right:0}}
                  onPress={()=>{this.setState({eyeIconCon:!this.state.eyeIconCon})}}
                /> 
              </Item>
              <Grid> 
                <Col >
                 <Text style={{width:"100%",fontSize:15,color:"rgb(48, 176, 252)"}} onPress={()=>{ this.setState({iniciarSession:false,registrarUsuario:true})}}>Registrar una cuenta</Text>
                </Col>
                <Col >
                 <Text style={{width:"100%",fontSize:15,color:"rgb(48, 176, 252)",textAlign:"right"}} onPress={()=>{ this.setState({iniciarSession:false,registrarUsuario:false})}}>He olvidado la contraseña</Text>
                </Col>
              </Grid>
              <Button  full  style={{backgroundColor:"#1E8ACE",marginTop:40}} onPress={this.login.bind(this)}><Text> INICIAR </Text></Button>
            </Body>
          </CardItem>
          <Text   style={{color:"#1E8ACE",fontSize:18,textAlign:"center",Button:0,marginTop:20}} onPress={()=>{ this.setState({sessionCorreoOusuario : !this.state.sessionCorreoOusuario})}}> {this.state.sessionCorreoOusuario ? 'Iniciar sesión con número de teléfonico.' :'Iniciar sesión con tu usuario correo electronico' } </Text>

        </Card>
        
      </Content>
    </Container> 
      :
    <SelectCodigoPais 
       codigoPaisDefecto="+51" 
       openCodePais={this.state.openSelectPais} 
       cerrarCodePais={this.cambiarEstadoModal.bind(this)}
       valorSeleccionado={this.valorSeleccionado.bind(this)}/>}
    
    
  </>
   : 
   <RegistrarUsuariooActualizacion registro={this.state.registrarUsuario} tipo={1} atras={this.atras.bind(this,0)} actualizarSession={this.iniciarSession.bind(this)}/>}
</>
) 

 
}
}