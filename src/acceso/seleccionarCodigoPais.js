import React, { Component } from 'react';
import { CountrySelection } from 'react-native-country-list';

export default class seleccionarCodigoPais extends Component {
  constructor(props){
    super(props);
   
    this.state = {
      selected:"+51",
    };  
    this.onCountrySelection=this.onCountrySelection.bind(this);
  }
  componentDidMount(){ 

  }

  cerrarModal(){
    this.props.cerrarCodePais();
  }
  onCountrySelection(val){
    this.props.valorSeleccionado({
      "dial_code" : val.callingCode
    });
    this.props.cerrarCodePais();
    
  }
 
  render() {

    const { selected } = this.state;
           
    return (
      <CountrySelection style={{height:"100%"}} action={(item) => this.onCountrySelection(item)} selected={selected}/>

    );
  }
}
