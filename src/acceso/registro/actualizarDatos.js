import React, { Component } from 'react';
import {Content,Card,CardItem,Container
,Item,Input ,Toast,Label,Right,Text,Icon,Button,Body,Form} from 'native-base';
import {Alert,View} from 'react-native';
import {HTTP} from '../../../axios';
import ComponentHeader from '../../header';
import SelectCodigoPais from '../seleccionarCodigoPais';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import {AsyncStorage} from 'react-native';
import IconEntypo from 'react-native-vector-icons/Entypo';
import Spinner from 'react-native-loading-spinner-overlay';
export default class actualizarDatos extends Component {
  constructor(props){
    super(props);
    this.state = {
      cancelarCont:true,
      hora:'',
      codigoVerificacion:'',
      loaderCodigoVerificacion:false,
      opcionSeleccionada:0,
      verificarCodigo :false,
      contador:0,
      valEdit:null,
      valPais:null,
      spinner:false,
      selectedPais:{
        "dial_code":51
      },
      disabledControl:true,
      usuario :null,
      correo :null,
      telefono :null,
      openSelectPais:false
    }
    this.changeInput=this.changeInput.bind(this);
    this.obtenerUsuario=this.obtenerUsuario.bind(this);
    this.verificarCodigo=this.verificarCodigo.bind(this);
    this.conteoContador=this.conteoContador.bind(this);
  }
  async componentDidMount(){
  
    this.obtenerUsuario();
    this.conteoContador();
  }
  async conteoContador(){
    const temp=setInterval(async () => {
      if(this.state.contador<1 ||  this.state.cancelarCont){
        await clearInterval(temp);
        return false;
      }
      await this.setState({
        contador:(this.state.contador-1)
      });
      const min=parseInt(this.state.contador/60);
      const seg=Math.round((this.state.contador/60-min)*60);
      //var pruebaFecha = moment(this.state.contador).format("DD-MM-YYYY HH:mm:ss");
      
      await this.setState({
        hora:(min+":"+seg)
      });
    }, 1000);
  }
  async obtenerUsuario(){
    try {
      const usuario=await HTTP('GET',`seguridad/buscarUsuario`,{ });
      const {data}=usuario;   
      if(data.estado){
        this.setState({usuario:data.data.nombre});
        this.setState({correo:data.data.correo});
       
        if(data.data.telefono){
         this.setState({telefono: data.data.telefono.split('-')[1] }); 
         this.setState({valPais: data.data.telefono.split('-')[0] });
        }

      }else{
        Toast.show({
          text: "No se pudo encontrar al usuario.",
          position: "bottom",
          duration: 5000
        });        
      } 
    } catch (error) {
      
    }

  }
  atras(val){
    this.props.atras(val);   
  }
 
  async changeInput(target){
    var keys = await Object.keys(target);
    await this.setState(target);
    
   if((this.state.usuario=="" || this.state.usuario==null ) || (this.state.correo=="" || this.state.correo==null ) || ( this.state.contraseniaConfirmacion==""|| this.state.contraseniaConfirmacion==null) ){
    await  this.setState({
      disabledControl : true
    });
   }else{
    await this.setState({
      disabledControl : false
    });
   }
  }

  async verificarCodigo(){
    if(!this.state.codigoVerificacion){
      return;
    }
    try {
      let valorIden=this.state.valEdit;
      if(this.state.opcionSeleccionada=="EDITAR_TELEFONO"){
        valorIden=(this.state.valPais ? this.state.valPais : 51) +"-"+valorIden;
      }
      let codigoVerificacion=this.state.codigoVerificacion;
      this.setState({ loaderCodigoVerificacion: true }); 
      const valor=await HTTP('GET',`seguridad/verificarCodigo?codigoIdentificacion=${valorIden}&codigoVerificacion=${codigoVerificacion}`,{ });
      const {data}=valor;
      if(data.estado){
        const actualizar=await HTTP('PUT',`seguridad/actualizarUsuario`,{
            "valor":valorIden,
            "tipo":this.state.opcionSeleccionada=="EDITAR_TELEFONO" ? "TELEFONO" : (this.state.opcionSeleccionada=="EDITAR_CORREO" ? "CORREO" : "USUARIO"),
        });
        const resultactualizacion=actualizar.data;
        if(resultactualizacion.estado){
          await this.obtenerUsuario();
          let datos=await AsyncStorage.getItem("datos");
          let dto=JSON.parse(datos); 
          
          if(this.state.opcionSeleccionada=="EDITAR_TELEFONO"){
            dto.telefono=valorIden;
          }
          if(this.state.opcionSeleccionada=="EDITAR_CORREO"){
            dto.correo=valorIden;
          }
          if(this.state.opcionSeleccionada=="EDITAR_USUARIO"){
            dto.usuario=valorIden;
          }
          await AsyncStorage.setItem("datos",JSON.stringify(dto));
          Alert.alert(
            'Actualización de Datos',
            resultactualizacion.mensaje,
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
          );          
        }else{
          Toast.show({
            text: resultactualizacion.mensaje,
            position: "bottom",
            duration: 5000
          });  
        }
          
       }else{
          Toast.show({
           text: data.mensaje,
           position: "bottom",
           duration: 5000
          });   
      }

    } catch (error) {
      Toast.show({
        text: "Error inesperado.",
        position: "bottom",
        duration: 5000
      }); 
    }
    await this.setState({
      opcionSeleccionada:0,verificarCodigo : false
    });
    await this.setState({loaderCodigoVerificacion: false });
  
  }
  async obtenerCodigo(){
    await this.setState({
      errorMesajeVal:null 
    });
    let valorIdentificador=this.state.valEdit;
    if(valorIdentificador==null || valorIdentificador==""){
      await this.setState({
        errorMesajeVal:this.state.opcionSeleccionada=="EDITAR_TELEFONO" ? 'Se requiere de teléfonico.' :(this.state.opcionSeleccionada=="EDITAR_CORREO" ? "Se requiere de correo." : "Se requiere de usuario.") 
      });
      return false;
    }

    if(this.state.opcionSeleccionada=="EDITAR_TELEFONO"){
      valorIdentificador=this.state.valPais ? this.state.valPais+"-"+valorIdentificador: 51+"-"+valorIdentificador;
    }


    if(this.state.opcionSeleccionada=="EDITAR_CORREO"){

      var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      var validacionCorreo=valorIdentificador.match(reg);
      if(!validacionCorreo){
        await this.setState({
          errorMesajeVal:'formato invalido de correo electronico.'
        });

        return false;
      }
    }
    //!
    let dtosUsuario=JSON.parse(await AsyncStorage.getItem("datos")); 
    if(this.state.opcionSeleccionada=="EDITAR_TELEFONO"){
      if(dtosUsuario.telefono==valorIdentificador){
        Toast.show({
          text: "El numero teléfonico ya esta verificado por el usuario.",
          position: "bottom",
          duration: 5000
        });        
        return false;
      }
    }
    if(this.state.opcionSeleccionada=="EDITAR_CORREO"){
      if(dtosUsuario.telefono==valorIdentificador){
        Toast.show({
          text: "El correo electronico ya se verificado por el usuario.",
          position: "bottom",
          duration: 5000
        });        
        return false;
      }
    }
    await this.setState({loaderCodigoVerificacion: true });
    try {
      const tipoActiv=this.state.opcionSeleccionada=="EDITAR_CORREO" ? 2 :(this.state.opcionSeleccionada=="EDITAR_TELEFONO" ?1 : 2);
      const sql=`seguridad/codigoActivacion?codigoIdentificacion=${valorIdentificador}&tipoActivacion=${tipoActiv}&registrarUsuario=1`;
      const valorVerificacion=await HTTP('GET',sql,{ });  
      const {data}=valorVerificacion;
      await this.setState({loaderCodigoVerificacion: false });

      if(!data.estado){
        if(this.state.opcionSeleccionada=="EDITAR_CORREO" || this.state.opcionSeleccionada=="EDITAR_TELEFONO"){
          if(data.tiempoQueda!=undefined){
            await this.setState({ verificarCodigo: true});
            await this.setState({cancelarCont:true});
            await this.setState({ contador: parseInt(data.tiempoQueda)});
            await this.setState({cancelarCont:false});
            await this.conteoContador();
            Toast.show({
              text: "Se ha enviado código de verificación.",
               position: "bottom",
               duration: 5000
            });  
           }
        }else{
          Alert.alert(
            'verificacion',
            'Se actualizo el nombre de usuario.',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
          );    
        }

      }else{
        if(this.state.opcionSeleccionada=="EDITAR_CORREO" || this.state.opcionSeleccionada=="EDITAR_TELEFONO"){
          await this.setState({ verificarCodigo: true});
          await this.setState({cancelarCont:true});
          await this.setState({ contador: parseInt(data.tiempoQueda)});
          await this.setState({cancelarCont:false});
          await this.conteoContador();
        }
        Toast.show({
          text: data.mensaje,
          position: "bottom",
          duration: 5000
        });
      }

    } catch (error) {
      this.setState({ loader: false });
      Toast.show({
        text: ""+JSON.stringify(error),
        position: "bottom",
        duration: 5000
      });     
    }
  
  }
  atrasEditar(){
    this.obtenerUsuario();
    this.setState({opcionSeleccionada :0});
  }
  async actualizarNombreUsuario(){
    try {
      let valorIden=this.state.valEdit;
      const actualizar=await HTTP('PUT',`seguridad/actualizarUsuario`,{
        "valor":valorIden,
        "tipo":this.state.opcionSeleccionada=="EDITAR_TELEFONO" ? "TELEFONO" : (this.state.opcionSeleccionada=="EDITAR_CORREO" ? "CORREO" : "USUARIO"),
      });
      const resultactualizacion=actualizar.data;
      if(resultactualizacion.estado){
        await this.obtenerUsuario();
        let datos=await AsyncStorage.getItem("datos");
        let dto=JSON.parse(datos); 
      if(this.state.opcionSeleccionada=="EDITAR_USUARIO"){
        dto.usuario=valorIden;
      }
      await AsyncStorage.setItem("datos",JSON.stringify(dto));
      await this.setState({
        opcionSeleccionada:0,verificarCodigo : false
      });
      await this.setState({loaderCodigoVerificacion: false });
      Alert.alert(
        'Actualización de Datos',
        resultactualizacion.mensaje,
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
      );
    }
    } catch (error) {
      Toast.show({
        text: "Error inesperado.",
        position: "bottom",
        duration: 5000
      }); 
    }
  }
render() {
  if(this.state.openSelectPais){
    return(<SelectCodigoPais 
      codigoPaisDefecto="+51" 
      openCodePais={this.state.openSelectPais} 
      cerrarCodePais={()=>{
        this.setState({
          openSelectPais:!this.state.openSelectPais,
        });
      }}
      valorSeleccionado={(val)=>{
        this.setState({
          valPais:val.dial_code
        });
      }}/>)

  }else{
    if(this.state.verificarCodigo){
      return(
      <>
         <ComponentHeader titulo={'editar usuario'} atras={async ()=>{
            await this.setState({ verificarCodigo: false});
         }}  isListo={false}/>
            <Body style={{marginTop: 30}}>
                <Content style={{width:"100%"}}>
                  <Form style={{width:"100%"}}>
                    <Item inlineLabel last>
                      <Input 
                        placeholder={'código de confirmación'}
                        name="codigoVerificacion"  
                        keyboardType={'numeric'}
                        value={this.state.codigoVerificacion}  onChangeText={(codigoVerificacion) => {this.setState({codigoVerificacion});} } />                  
                    </Item>
                    <Text style={{marginLeft:15,marginTop:8}} note>Obtener de nuevo en ({this.state.hora }) <Text style={{marginLeft:15,marginTop:8,color:"#2944AB"}} note onPress={this.obtenerCodigo.bind(this)}> Generar nuevo codigo</Text>.</Text>
                    <View style={{width:"100%",flex: 1,flexDirection: 'column',alignItems: 'flex-end'}}>
                        <Button disabled={this.state.codigoVerificacion==null ||this.state.codigoVerificacion=="" ? true : false}  style={{marginTop:20,height:35,width:"100%"}} onPress={this.verificarCodigo.bind(this)} disabled={(this.state.codigoVerificacion==null || this.state.codigoVerificacion=="")} block>
                          <Text style={{textAlign:'center',width:"100%"}}>Verificar código</Text>
                        </Button>
                    </View>
                  </Form>
                </Content>
            </Body>
      </> );
    }
    if(!this.state.verificarCodigo && ( this.state.opcionSeleccionada=="EDITAR_USUARIO" || this.state.opcionSeleccionada=="EDITAR_CORREO"  || this.state.opcionSeleccionada=="EDITAR_TELEFONO") ){
    return( 
    <>
    <ComponentHeader titulo={'editar usuario'} atras={this.atrasEditar.bind(this) } 
    isListo={this.state.opcionSeleccionada=="EDITAR_USUARIO" ? true : false} listo={this.actualizarNombreUsuario.bind(this)}/>
    <Spinner
          visible={this.state.loaderCodigoVerificacion}
          textContent={'cargando...'}
          textStyle={{
            color:"#fff"
          }}
    />
      <Card padder style={{height:'100%'}}>
       {
        this.state.opcionSeleccionada=="EDITAR_TELEFONO" ?
        <>
        
            <Label style={{marginLeft:12}} block>código del País</Label>  
            <Item   onPress={()=>{
              this.setState({
                openSelectPais:!this.state.openSelectPais,
              });
            }}> 
                <Text  style={{fontSize:15}}>{this.state.valPais ? ("+"+this.state.valPais)  : "51"}</Text>
               <Right>
                 <Icon style={{fontSize:15}} name="arrow-down" />
               </Right>
             
             </Item>
                          
         </> : null
      }
     <Item error={this.state.valEdit=="" ? true : (this.state.errorMesajeVal ? true : false)}>
        <IconEntypo 
          name={this.state.opcionSeleccionada=="EDITAR_USUARIO" ? "user": (this.state.opcionSeleccionada=="EDITAR_CORREO"  ? "mail" : "phone") } 
          color="#000000"
          size={18} 
        />
      <Input   
        maxLength={this.state.opcionSeleccionada=="EDITAR_USUARIO" ? 40 : (this.state.opcionSeleccionada=="EDITAR_CORREO" ? 72 : 25)} placeholder={this.state.opcionSeleccionada=="EDITAR_CORREO" ? 'correo electronico' :(this.state.opcionSeleccionada!="EDITAR_USUARIO" ? 'nombre de usuario' : 'teléfono celular' ) } 
        value={this.state.valEdit} 
        onChangeText={(valEdit)=>{this.changeInput({valEdit})} }
        keyboardType={this.state.opcionSeleccionada=="EDITAR_USUARIO" ? 'default' :(this.state.opcionSeleccionada=="EDITAR_CORREO" ? "email-address" : "numeric") }/>
      </Item>
      <Text style={{color:"red"}}>{this.state.errorMesajeVal}</Text>
      {this.state.opcionSeleccionada!="EDITAR_USUARIO" ?  
      <Button disabled={this.state.valEdit==null || this.state.valEdit=="" ? true : false} style={{marginTop:20,height:35,width:"100%"}} 
      onPress={this.obtenerCodigo.bind(this)}><Text style={{textAlign:'center',width:"100%"}}>{this.state.opcionSeleccionada=="EDITAR_CORREO" ? 'Comprobar correo electronico' :'Comprobar numero telefonico'}</Text></Button> : null}
     
    </Card>
    </>
    )
  }else{
    if(this.state.opcionSeleccionada==0){
      return (
        <Container>
           <ComponentHeader titulo="Actualizar Datos"  atras={this.atras.bind(this)} />
            <Card padder style={{height:'100%'}}>
            <Spinner
              visible={this.state.loader}
              textContent={'Loading...'}
              textStyle={{
                color: '#FFF'
              }}
            />
                <CardItem>
                  <Content>
                    <Item    error={this.state.usuario=='' ? !this.state.usuario : null}>
                      <Label>Usuario</Label>
                      <Input  editable={false} value={this.state.usuario} onChangeText={(usuario)=>{this.changeInput({usuario})} }/>
                      <IconAntDesign 
                        name={"edit"} 
                        color="#000000"
                        size={18}
                        onPress={()=>{this.setState({opcionSeleccionada : "EDITAR_USUARIO"});this.setState({valEdit:this.state.usuario }); }}
                      />
                    </Item>
                    <Item  error={this.state.correo=='' ? !this.state.correo : null}>
                     <Label >correo</Label>
                     <Input editable={false} value={this.state.correo} onChangeText={(correo)=>{this.changeInput({correo})} }/>
                      <IconAntDesign 
                        name={"edit"} 
                        color="#000000"
                        size={18}
                        onPress={()=>{this.setState({opcionSeleccionada : "EDITAR_CORREO"});this.setState({valEdit:this.state.correo });}}
                       />
                    </Item> 
                     <Item   error={this.state.errorMesajeVal!=null}>
                        <Label>teléfono celular</Label>
                        <Input editable={false} value={this.state.telefono ? this.state.telefono.replace("-",""):null}  onChangeText={(telefono) =>{
                          this.setState({valPais : telefono ? telefono.split('-')[0] : null })
                          this.setState({telefono : telefono ? telefono.split('-')[1] : null })

                        }  } />               
                        <IconAntDesign 
                         name={"edit"} 
                         color="#000000"
                         size={18}
                         onPress={()=>{this.setState({opcionSeleccionada : "EDITAR_TELEFONO"});this.setState({valEdit:this.state.telefono });}}
                        />
                     </Item>
  
                  </Content>
                </CardItem>
    
                <Content  />
              </Card>
          </Container>
      );  
    }
  
    
  }
}



  }
}
