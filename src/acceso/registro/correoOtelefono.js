import React, { Component } from "react";
import { Container, Content, Card, CardItem,Label,Icon, Text, Body,Button,Form,Item,Input, Right,Toast,Left } from "native-base";
import {View,AsyncStorage,Alert} from 'react-native';
import {HTTP} from '../../../axios';
import Spinner from 'react-native-loading-spinner-overlay';
import { Col, Grid } from 'react-native-easy-grid';
import SelectCodigoPais from '../seleccionarCodigoPais';
import IconFeather from 'react-native-vector-icons/Feather';
import ComponentHeader from '../../header';
export default class CardItemButton extends Component {
  constructor(props){
    super(props)
    this.state={
      registroTelefono:false,
      eyeIconConConf:false,
      eyeIconCon:false,
      disabledControl:true,
      errorContraseniaConfirmacion:false,
      errorContrasenia:false,
      contrasenia:null,
      contraseniaConfirmacion:null,
      nombre:null,
      openSelectPais:false,
      selectedPais:{
        "dial_code":51
      },
      codigoVerificacion:null,
      loader:false, 
      errorMesajeVal:null,
      valorIdentificador:null,
      cancelar:false,
      selection:0,
      contador:900,
      hora:''
     // fechaActual: new Date()
    };
  }
  async componentDidMount(){
    this.conteoContador();
  
  }
  vistaDondeEsta(){
    return this.state.selection;
  }
  conteoContador(){
    const temp=setInterval(async () => {
      if(this.state.contador<1 ||  this.state.cancelar){
        await clearInterval(temp);
        return false;
      }
      await this.setState({
        contador:(this.state.contador-1)
      });
      const min=parseInt(this.state.contador/60);
      const seg=Math.round((this.state.contador/60-min)*60);
      //var pruebaFecha = moment(this.state.contador).format("DD-MM-YYYY HH:mm:ss");
      
      await this.setState({
        hora:(min+":"+seg)
      });
    }, 1000);
  }
  async changeValor(target){
    await this.setState(target);
    
    var keys = await Object.keys(target);
    await this.setState(target);
    //this.state.errorMesajeVal
    Toast.show({
      text: keys,
      position: "bottom",
      duration: 5000
    });

    /*if(keys=="valorIdentificador"){
      this.setState({
        errorMesajeVal:null
      });
    }*/
    
    if(keys=="contraseniaActual"){
      await this.setState({
        errorContrasenia :!this.state.errorContrasenia
      });
    }
    if(keys=="contraseniaConfirmacion"){
      await this.setState({
        errorContraseniaConfirmacion :!this.state.errorContraseniaConfirmacion
      });
    }
    if((this.state.contrasenia=="" || this.state.contrasenia==null ) 
    || (this.state.contraseniaConfirmacion=="" || this.state.contraseniaConfirmacion==null ) 
     ){
      
      await  this.setState({
        disabledControl : true
      });

     }else{
      await this.setState({
        disabledControl : false
      });
     }
  }
  async obtenerCodigo(){
    let valorIdentificador=this.state.valorIdentificador;
    if(this.state.valorIdentificador==null || this.state.valorIdentificador==""){
      //this.state.errorMesajeVal
      await this.setState({
        errorMesajeVal:this.state.registroTelefono ? 'Se requiere de teléfono.' :'Se requiere de correo.' 
      });
      return false;
    }
    if(this.state.registroTelefono){
      valorIdentificador=this.state.selectedPais.dial_code+"-"+this.state.valorIdentificador;
    }
    if(!this.state.registroTelefono){

      var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      var validacionCorreo=this.state.valorIdentificador.match(reg);
      if(!validacionCorreo){
        await this.setState({
          errorMesajeVal:'formato invalido de correo electronico.'
        });
        return false;
      }
      await this.setState({
        errorMesajeVal:null
      });
     

    }
    try {             
      this.setState({ loader: true }); 
      const valor=await HTTP('GET',`seguridad/codigoActivacion?codigoIdentificacion=${valorIdentificador}&tipoActivacion=${this.state.registroTelefono ? 1 : 2 }&registrarUsuario=${this.props.registro==true ? 1 : 0}`,{ });
      const {data}=valor;        
      if(data.estado){
        await this.setState({cancelar:true});
        await this.setState({ contador: parseInt(data.tiempoQueda)});
        await this.setState({cancelar:false});
        await this.setState({ selection: 1,loader: false });
      }else{ 
        if(data.tiempoQueda!=undefined){
          await this.setState({cancelar:true});
          await this.setState({ contador: parseInt(data.tiempoQueda)});
          await this.setState({cancelar:false});
          await this.setState({ selection: 1,loader: false });
          Toast.show({
            text: data.mensaje,
            position: "bottom",
            duration: 5000
          });
        }else{
          Toast.show({
            text: data.mensaje,
            position: "bottom",
            duration: 5000
          });
        }
      }
      await this.setState({loader: false });
      await this.setState({codigoVerificacion:""});
    } catch (error) {
      this.setState({ loader: false });
      Toast.show({
        text: ""+JSON.stringify(error),
        position: "bottom",
        duration: 5000
      });     
    }
  
  }
  async atras(){
    if(this.state.selection==0){
     this.props.atras(0);
    }else{
      this.setState({
        selection:0
      });
    }

  }
  visualizar(val){
    this.setState(val);    
  }
  cambiarEstadoModal() {
    this.setState({
      openSelectPais:!this.state.openSelectPais,
    });

  }
  valorSeleccionado(val){
    this.setState({
      selectedPais:val
    });
  }
  async verificarCodigo(){
    try {
      this.setState({ loader: true }); 
      let valorIdentificador=this.state.valorIdentificador;
      if(this.state.registroTelefono==1){
        valorIdentificador=this.state.selectedPais.dial_code+"-"+this.state.valorIdentificador;
      }
      const valor=await HTTP('GET',`seguridad/verificarCodigo?codigoIdentificacion=${valorIdentificador}&codigoVerificacion=${this.state.codigoVerificacion}`,{ });
      const {data}=valor;
      if(data.estado){
        this.setState({
          selection:2
        });
        Toast.show({
          text: "verificación exitosa.",
          position: "bottom",
          duration: 5000
        }); 
          
      }else{
        Toast.show({
          text: "codigo invalido.",
          position: "bottom",
          duration: 5000
        });   
      }

    } catch (error) {
      Toast.show({
        text: "Error inesperado.",
        position: "bottom",
        duration: 5000
      }); 
    }
    await this.setState({loader: false });
  }
  async registrar(){
    if(!(!this.state.contrasenia || !this.state.contraseniaConfirmacion )){
      if(this.state.contrasenia.length<6){
        Toast.show({
          text: "La cantidad minima de caracteres es de 6.",
          position: "bottom",
          duration: 5000
        });  
      }
      if(this.state.contrasenia!=this.state.contraseniaConfirmacion){
        Toast.show({
          text: "Asegúrate de que ambas contraseñas coincidan.",
          position: "bottom",
          duration: 5000
        });  
      }
    }
    try {
      this.setState({loader : false});
      let valor=null;  
      valorIdentificador= this.state.valorIdentificador;
      if(this.state.registroTelefono==1){
        valorIdentificador=this.state.selectedPais.dial_code+"-"+this.state.valorIdentificador;
      }
      if(this.props.registro){
        valor=await HTTP('POST',`seguridad/crearUsuario`,{
          "nombre":this.state.nombre,
          "valorTipo":valorIdentificador,
          "contrasenia":this.state.contrasenia,
          "contraseniaVerificacion":this.state.contraseniaConfirmacion,
          "tipo":this.state.registroTelefono ? 1 : 2
        });
      }else{
        const restable={
          "nombre":this.state.nombre,
          "valor":valorIdentificador,
          "contrasenia":this.state.contrasenia,
          "contraseniaVerificacion":this.state.contraseniaConfirmacion,
          "tipo":this.state.registroTelefono ? 3 : 2
        };
        valor=await HTTP('PUT',`seguridad/restablecerUsuario`,restable);
      }

      let {data}=valor;

      if(data.estado){
        let usuario=this.state.valorIdentificador;
        if(this.state.registroTelefono){
           usuario=this.state.selectedPais.dial_code+"-"+this.state.valorIdentificador;

        }
        responseLogueo=await HTTP('POST',`seguridad/logueo`,{
          "nombre":this.state.nombre,
          "valorTipo":usuario,
          "contrasenia":this.state.contraseniaConfirmacion,
          "tipo":this.state.registroTelefono ? 3 : 2
        });

        const valorLogeo=responseLogueo.data;      
        if(valorLogeo.estado){
          let dataLogeo=valorLogeo.data;
          await AsyncStorage.setItem('tokenUsuario',dataLogeo.token);
          await AsyncStorage.setItem('datos',JSON.stringify(dataLogeo));  
          Alert.alert(
            this.props.registro ? 'usuario' : 'restablecer contraseña',
            this.props.registro ? 'Se creo su cuenta exitosamente.' :'La contraseña se ha restablecido con éxito.',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
          );
          
         this.props.actualizarSession();
        }else{
          Toast.show({
            text: valorLogeo.mensaje,
            position: "bottom",
            duration: 5000
          }); 
          
        }

      }else{
        Toast.show({
          text: data.mensaje,
          position: "bottom",
          duration: 5000
        });
      }
     } catch (error) {
        Toast.show({
         text: JSON.stringify(error),
         position: "bottom",
         duration: 5000
       });  
    }
    this.setState({loader : false});

  }
  
  render() {
    
 
   if(this.state.openSelectPais){
    return (
    <SelectCodigoPais 
      codigoPaisDefecto="+51" 
      openCodePais={this.state.openSelectPais} 
      cerrarCodePais={this.cambiarEstadoModal.bind(this)}
      valorSeleccionado={this.valorSeleccionado.bind(this)}/>)
   }else{
    return (
      <Container>
        <ComponentHeader titulo={this.props.registro ?  'registro de cuenta' : 'restablecer contraseña'} atras={this.atras.bind(this)}/>
        <Content padder style={{backgroundColor:"#DFE7EF" ,height:"100%"
         }}>

        <Spinner
          visible={this.state.loader}
          textContent={'Loading...'}
          textStyle={{
            color:"#fff"
          }}
        />
          <Card style={{
            marginTop: 50,
            width: "100%",
            height: "100%",
           }}>
         
             {
             this.state.selection==0 
              
             ?
           
             <Body style={{marginTop: 30}}>
               <Content style={{width:"100%"}}>
                 <Form style={{width:"100%"}}>
                   {
                     this.state.registroTelefono==1 ? 
                     <Grid>
                       <Col style={{marginTop:29}}>
                         <Label style={{marginLeft:12}} block>código del País</Label>  
                         <Item   onPress={this.cambiarEstadoModal.bind(this)}> 
                           <Left>
                             <Text  style={{fontSize:15}}>{this.state.selectedPais ? ("+"+this.state.selectedPais.dial_code)  : ""}</Text>
                           </Left>  
                           <Right>
                             <Icon style={{fontSize:15}} name="arrow-down" />
                           </Right>
                         </Item>
                     </Col>                       
                  </Grid>
                   : 
                    <></>
                   }
                  <Item  error={this.state.errorMesajeVal!=null}>
                    <Input placeholder={this.state.registroTelefono==1 ?  'teléfono celular' :'correo electrónico'} name="contraseniaNueva"   value={this.state.valorIdentificador}  onChangeText={(valorIdentificador) => this.changeValor({valorIdentificador})} />               
                  </Item>
                  <Text style={{marginLeft:15,marginTop:3,color:"red"}} note >{this.state.errorMesajeVal!=null ? this.state.errorMesajeVal : ''}</Text>
                  <Text style={{marginLeft:15,marginTop:8}} note>La direccion de correo electronico se utiliza para iniciar sesión o recuperar la contraseña.</Text>
                  <View style={{width:"100%",flex: 1,flexDirection: 'column',alignItems: 'flex-end'}}>
                    <Button primary  style={{marginTop:20,height:35,width:"100%"}} onPress={this.obtenerCodigo.bind(this,1)}><Text style={{textAlign:'center'}}>Obtener código de seguridad</Text></Button>
                  </View>

                </Form>
                 <Text    onPress={()=>{ this.setState({registroTelefono : !this.state.registroTelefono});this.setState({errorMesajeVal:null});this.setState({valorIdentificador:''}); }}> {this.state.registroTelefono ? 'Registro con tu correo electronico': 'Registro con tu número de teléfono.'  } </Text>              

               </Content>

              </Body>
            
              :
              (this.state.selection==1 ? 
          
                <Body style={{marginTop: 30}}>
                  <Content style={{width:"100%"}}>
                    <Form style={{width:"100%"}}>
                      <Item inlineLabel last>
                      
                        <Input 
                        placeholder={'código de confirmación'}
                        name="codigoVerificacion"  
                        keyboardType={'numeric'}
                        value={this.state.codigoVerificacion}  onChangeText={(codigoVerificacion) => this.changeValor({codigoVerificacion})} />                  
                      </Item>
                      <Text style={{marginLeft:15,marginTop:8}} note>Obtener de nuevo en ({this.state.hora }) <Text style={{marginLeft:15,marginTop:8,color:"#2944AB"}} note onPress={this.obtenerCodigo.bind(this)}> Generar nuevo codigo</Text>.</Text>
                      <View style={{width:"100%",flex: 1,flexDirection: 'column',alignItems: 'flex-end'}}>
                        <Button   style={{marginTop:20,height:35,width:"100%"}} onPress={this.verificarCodigo.bind(this)} disabled={(this.state.codigoVerificacion==null || this.state.codigoVerificacion=="")} block>
                          <Text style={{textAlign:'center',width:"100%"}}>Verificar código</Text>
                        </Button>
                      </View>
                    </Form>
                  </Content>
                </Body> 
                 :
              <CardItem>
                <Body style={{marginTop: 30}}>
                <Content style={{width:"100%"}}>
                  <Form style={{width:"100%"}}>
                    <Item  inlineLabel last error={this.state.contrasenia=='' ? !this.state.contrasenia : null}> 
                        <Input  secureTextEntry={!this.state.eyeIconCon} placeholder='contraseña' value={this.state.contrasenia}  onChangeText={(contrasenia) => this.changeValor({contrasenia})}/>
                        <IconFeather
                          name={this.state.eyeIconCon ? "eye-off"   :"eye"}
                          color="#000000"
                          size={25}
                          style={{right:0}}
                          onPress={this.visualizar.bind(this,{eyeIconCon:!this.state.eyeIconCon})}
                        />  
                    </Item>
                    <Item last error={this.state.contraseniaConfirmacion=='' ? !this.state.contraseniaConfirmacion : null}>
                      <Input secureTextEntry={!this.state.eyeIconConConf} placeholder='contraseña confirmación' value={this.state.contraseniaConfirmacion}  onChangeText={(contraseniaConfirmacion) => this.changeValor({contraseniaConfirmacion})}/>
                      <IconFeather
                        name={this.state.eyeIconConConf ?  "eye-off"   :"eye"}
                        color="#000000"
                        size={25}
                        style={{right:0}}
                        onPress={this.visualizar.bind(this,{eyeIconConConf:!this.state.eyeIconConConf})}
                      /> 
                    </Item>
                    <View style={{width:"100%",flex: 1,flexDirection: 'column',alignItems: 'flex-end',marginTop:30}}>
                      <Button   onPress={this.registrar.bind(this,1)} disabled={!this.state.contrasenia || !this.state.contraseniaConfirmacion } block>
                      <Text style={{textAlign:'center',width:"100%"}}>{this.props.registro ?  'registro de cuenta' : 'restablecer contraseña'}</Text></Button>
                    </View>
                  </Form>
                </Content>
                </Body> 
              </CardItem>)  
                       
             }

            
        </Card>
      </Content>
    </Container>
    );
   }

  }
}