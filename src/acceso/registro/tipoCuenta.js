import React, { Component } from "react";
import { Container, Content, Card, CardItem, Text, Body,Button } from "native-base";
import IconAntDesign from 'react-native-vector-icons/AntDesign';

import ComponentHeader from '../../header';
import CorreoOtelefono from '../registro/correoOtelefono';
export default class tipoCuenta extends Component {
  constructor(props){
   super(props);
   this.state={
     selection:0,
   }

  }
  registro(val){
    if(this.state.selection==val && val==0){
      this.props.selectOpcion(0);
    }
    this.setState({selection:val});
  }
  iniciarSession(){
    this.props.iniciarSession();
  }
  render() {
    return (
      <Container>
        {
          this.state.selection==0 ?
        <>
         <ComponentHeader titulo={'registro de cuenta'} atras={this.registro.bind(this)}/>
 
          <Content padder style={{backgroundColor:"#DFE7EF" ,height:"100%"
        }}>

          <Card style={{
           marginTop: 50,
           width: "100%",
           height: "100%",
          }}>
          
           <CardItem button>
             <Body style={{marginTop: 30}}>
             <Button full info style={{marginBottom: 20}} onPress={this.registro.bind(this,1)}>
               <IconAntDesign 
                 style={{fontSize:20,position: 'absolute',left:0}}             
                 name={"mobile1"} 
                 color="#000000"
                 size={18}/>
               <Text  style={{padding:5,marginLeft:5,textAlign:"justify"}}>Numero de telefono Movil</Text></Button>
             <Button full light onPress={this.registro.bind(this,2)} >
             <IconAntDesign      
               name={"mail"}
               color="#000000"
               style={{fontSize:20,position: 'absolute',left:0}}/>
                 <Text style={{padding:5,marginLeft:5,textAlign:"justify"}}>Direccion de correo electronicos</Text></Button>
             </Body>
           </CardItem>

          </Card>
        </Content></> :
            <CorreoOtelefono tipo={this.state.selection} atras={this.registro.bind(this,0)} actualizarSession={this.iniciarSession.bind(this)}/>
        }


      </Container>
    );
  }
}