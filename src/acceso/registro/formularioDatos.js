import React, { Component } from "react";
import { Content, Card, CardItem,Label, Text, Body,Button,Form,Item,Input } from "native-base";
import {View} from 'react-native';
export default class CardItemButton extends Component {
  constructor(props){
    super(props);
    this.state={
      selection:0
    };
  }
  verificarCodigo(val){
    this.setState({
      selection:val
    });
  }
  render() {
    return (
        <Content padder style={{backgroundColor:"#DFE7EF" ,height:"100%"
         }}>
          <Card style={{
            marginTop: 50,
            width: "100%",
            height: "100%",
         }}>
          { this.state.selection==0 ? <CardItem >
              <Body style={{marginTop: 30}}>
              <Content style={{width:"100%"}}>
               <Form style={{width:"100%"}}>
               <Item floatingLabel>

                 <Label>{this.props.tipo==1 ?  'teléfono celular' :'correo electrónico'}</Label>
                 <Input />               
                </Item>
                <Text style={{marginLeft:15,marginTop:8}} note>La direccion de correo electronico se utiliza para iniciar sesión o recuperar la contraseña.</Text>

                 <View style={{width:"100%",flex: 1,flexDirection: 'column',alignItems: 'flex-end'}}>
                    <Button primary  style={{marginTop:20,height:35,width:"100%"}} onPress={this.verificarCodigo.bind(this,1)}><Text style={{textAlign:'center',width:"100%"}}>Obtener código de seguridad</Text></Button>
                </View>

               </Form>
              </Content>
              </Body>
            </CardItem>
 
           :
           <CardItem >
           <Body style={{marginTop: 30}}>
           <Content style={{width:"100%"}}>
            <Form style={{width:"100%"}}>
            <Item floatingLabel>

              <Label>{this.props.tipo==1 ?  'teléfono celular' :'correo electrónico'}</Label>
              <Input />               
             </Item>
             <Text style={{marginLeft:15,marginTop:8}} note>La direccion de correo electronico se utiliza para iniciar sesión o recuperar la contraseña.</Text>

              <View style={{width:"100%",flex: 1,flexDirection: 'column',alignItems: 'flex-end'}}>
                 <Button primary  style={{marginTop:20,height:35,width:"100%"}} onPress={this.verificarCodigo.bind(this,1)}><Text style={{textAlign:'center',width:"100%"}}>Obtener código de seguridad</Text></Button>
             </View>

            </Form>
           </Content>
           </Body>
         </CardItem>

        } 
            
          </Card>
        </Content>
    );
  }
}