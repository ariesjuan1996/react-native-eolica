import React, { Component } from 'react';
import { Container, Header,Footer, FooterTab, Button, Text,Right } from 'native-base';
import Ionicons from "react-native-vector-icons/Ionicons";
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import ActualizarContrazenia from './acceso/actualizarContrasenia';
import ContentDataDispositivos from './contents/contentDataDispositivos';
import ContenConfiguracion from './contents/contenConfiguracion';
import InstalarDispositivos from './contents/InstalarDispositivos';
import ActualizarDatos from '../src/acceso/registro/actualizarDatos';
import Reportes from './contents/reportes';
import { Alert } from "react-native";
import {AsyncStorage} from 'react-native';
export default class BadgeFooterTabsExample extends Component {
  constructor(props){
    super(props);
    this.state={
      tituloMenu:"Monitoreo del generador",
      selectContent:"MONITOREO",
      abrirScaner:false,
      NuevoDispositivo:false,
      opcionConfig:0
    }
  }
  componentDidMount(){
    //Alert.alert("siii");
  }
  selectContent(val){
    this.setState({
      selectContent :val
    });
  }
  async cerrarSession(){
    await this.props.cerrarSession();
  }
  async identificarProducto(dat){
    Alert.alert(
      'error de red',
      [
        {
          text: 'cancelar',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
       
      ],
      {cancelable: false},
    );
    if(dat=="undefined"){
      Alert.alert(
       'error de red',
       [
         {
           text: 'cancelar',
           onPress: () => console.log('Cancel Pressed'),
           style: 'cancel',
         },
        
       ],
       {cancelable: false},
     );
    }

    if(dat==null){
      Alert.alert(
        'Escaner producto',
        'producto no encontrado',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
         
        ],
        {cancelable: false},
      );
    }
  if(dat){
    this.setState({abrirScaner:!this.state.abrirScaner}); 
    let productos=await AsyncStorage.getItem("productos");
    if(productos==null){
        let listProductos=[];
        listProductos.push(dat);
        await AsyncStorage.setItem(JSON.stringify(listProductos));
      }else{
        let listProductos=JSON.parse(productos);
        let band=0;
        listProductos.forEach(element => {
          if(element.serial==dat.serial){
            band=1;
          }
        });
        if(band==0){
          listProductos.push(dat);
          await AsyncStorage.setItem(JSON.stringify(listProductos));

        }
      }
    
  }


  }
  cambioOpcion(val){
     this.setState({
      opcionConfig:val
     });
  }
  cerrar(){
    this.setState({ NuevoDispositivo: true });
  }
  render() {
    if(this.state.opcionConfig==0){
      return (
        <Container style={{height:"100%",backgroundColor:"#FAFAFA"}}>
        {this.state.NuevoDispositivo ? <InstalarDispositivos atras={() => {
                   this.setState({ NuevoDispositivo: false });
                 }} />  : 
        <>
        <Header style={{backgroundColor:"#2F3AA6",borderBottomColor:"#2F3AA6",borderBottomWidth:0.5,height : 50,width:"100%"}}>        
        <Text style={{textAlign:"center",marginTop:10,fontSize:22,width:"75%",color:"#FFFFFF"}}>{this.state.tituloMenu}</Text>
         
         {
           this.state.selectContent=="MONITOREO" ? 
             <Right>
               <Ionicons            
                 name={"md-add"}
                 color="#ffffff"
                 size={28}
                 style={{width:"25%",marginTop:10}}
                 onPress={() => {
                   this.setState({ NuevoDispositivo: true });
                 }}
           />  
         </Right> 
           :  <Text style={{width:"25%"}}></Text>
         } 
          </Header>

      {this.state.selectContent=="MONITOREO" ? <ContentDataDispositivos/> : 
      (this.state.selectContent=="REPORTES" ? <Reportes/> : <ContenConfiguracion cambioOpcion={this.cambioOpcion.bind(this)} cerrarSession={this.cerrarSession.bind(this)}/> )  }

      <Footer style={{borderTopColor:"#2F3AA6",borderTopWidth:0.8}}> 
        <FooterTab >
          <Button  style={{backgroundColor:"#2F3AA6"}}
           badge vertical onPress={(e)=>{
            this.selectContent("MONITOREO");
            this.setState({tituloMenu:"Monitoreo del generador"});
           }}>
          <MaterialCommunityIcons
           name={"monitor-cellphone"}
            color="#ffffff"
            size={25}
          />              
            <Text style={{color:"#ffffff"}}>Monitoreo</Text>
          </Button>
          <Button active badge vertical  onPress={()=>{
            this.selectContent("REPORTES");
            this.setState({tituloMenu:"Reporte del generador"});
          }}
            style={{backgroundColor:"#2F3AA6"}}>
            <IconMaterialIcons
              name={"devices"}
              color="#ffffff"
              size={25}
            />
            <Text style={{color:"#ffffff"}}>REPORTES</Text>
          </Button>
          <Button active badge vertical  onPress={()=>{
            this.selectContent("CONFIGURACION");
            this.setState({tituloMenu:"configuración"});
          }}
            style={{backgroundColor:"#2F3AA6"}}>
            <IconMaterialIcons
              name={"settings"}
              color="#ffffff"
              size={25}
            />
            <Text style={{color:"#ffffff"}}>configuración</Text>
          </Button>
        </FooterTab>
      </Footer>
       </> }
    </Container>
  
    );      

    }
   if(this.state.opcionConfig=="ACTUALIZAR"){
     return(<ActualizarContrazenia atras={()=>{
      this.setState({opcionConfig:0});
      this.selectContent.bind(this,"CONFIGURACION");
     }}/>  );
       
   }
   if(this.state.opcionConfig=="ACTUALIZAR_INFO"){
    return(<ActualizarDatos atras={()=>{
     this.setState({opcionConfig:0});
     this.selectContent.bind(this,"CONFIGURACION");
    }}/>  );
      
  }


  }
}